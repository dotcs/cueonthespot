# CueOnTheSpot
CueOnTheSpot is a Python based cue-sheet creator using the metadata provided by Spotify®.

To get everything working you need the following Python packages installed: `sys`, `os`, `getopt`, `urllib`, `HTMLParser`, `re`, `httplib2`, `BeautifulSoup`. Please check if you have installed them before using the program to avoid errors.

## Usage
Use this program to find albums on Spotify® in the first step and use the metadata to generate a cue-sheet in the second step. To find the albums you have several options. At the end you always need the internal identifier that Spotify® is using. The next sections cover how to get the identifier.

### How to search if artist and album name is known
It is possible to search a combination of artist and album information using the option `--search-album`. For example if you want to find information about the album "Immortal" by "Michael Jackson" you can use 

    python generator.py --search-album "Michael Jackson Immortal"

This will print results that matches with your search query. At the time of writing an excerpt of the results looks like this

    Immortal    Michael Jackson    spotify:album:5ReKddpdZKgpVlecQhLnEH
    Immortal    Michael Jackson    spotify:album:61MiqdH9BJ1BDuGP8o3Yfd

The first column shows the album title, the second column shows the artist name and the third column contains the information about the album identifier that we need to extract. If you do not know which of the identifier is the correct one (as in the case above) you can simply try one of them. Most of the time they are equal.

In the second step you can create your cue sheet. To do so you can use 

    python generator.py --album spotify:album:5ReKddpdZKgpVlecQhLnEH 

which will print the result to the terminal. You have multiple options to print the information to a file. You can now use one of the following two commands to generate your cue-sheet file 

    python generator.py --album spotify:album:5ReKddpdZKgpVlecQhLnEH > ~/output.cue
    python generator.py --album spotify:album:5ReKddpdZKgpVlecQhLnEH --output ~/Desktop/
    
Now you are done. Take a look at your cue-sheet file and convince yourself. That was easy!

### How to search stuff by artist
If you want to know which albums are available for a given artist you can use the `--search-artist` option. This will give you all results Spotify® could associate with your search query. As an example we will search for `Michael` in order to find albums of Michael Jackson.

    python generator.py --search-artist "Michael"
    
An excerpt of the results is the following

    Michael Jackson         spotify:artist:3fMbdgg4jU18AjLCKBhRSm
    Michael Bublé	        spotify:artist:1GxkXlMwML1oSg5eLPiAz3
    George Michael	        spotify:artist:19ra5tSw0tWufvUp8GotLo
    Michael Bolton	        spotify:artist:6YHEMoNPbcheiWS2haGzkn
    Michael Mind Project	spotify:artist:3I4ttwmYBwsR01fin6ItJR
    
So with every artist Spotify® connects an identifier. Use this identifier to find albums that includes this artist by using

    python generator.py --search-album spotify:artist:3fMbdgg4jU18AjLCKBhRSm
    
This will give you the result (again this is an excerpt)

    Michael Jackson: The Complete Remix Suite      *Unknown*            spotify:album:5GhKGrFaw4V1oSvnkHmn2s
    The Motown Collection	                       Michael Jackson	    spotify:album:5gKR6pby6LsHOTIRc3Xc77
    Bad 25th Anniversary	                       Michael Jackson	    spotify:album:24TAupSNVWSAHL0R7n71vm
    Bad 25th Anniversary (Deluxe)	               Michael Jackson	    spotify:album:1Ta6GYcPUNtFIymr55ZXpw
    
You can now use the same method as before to generate your cue-sheet, namely

    python generator.py --album spotify:album:5gKR6pby6LsHOTIRc3Xc77
    
Again this was simple, wasn't it?

### Generate a cue-sheet using only a few of the tracks on an album
You can exclude tracks on an album when generating the cue-sheet. Also you can only include some of the tracks and exclude all others. The parameters for this are `--exclude` and `--include`. Please note, that they can not be combined.

Both parameters can be used in the same way. You can give them several track numbers seperated by comma `,` or a range between a number `a` and `b` seperated by a colon, e.g. `5:9` is another way for writing `5,6,7,8,9`.

Let us make an example and exclude the tracks `5,6,10,14,15,16,17,19` from an album. Let us assume that the album identifier is given by `$ALBUM_ID`. We can then use

    python generator.py --album $ALBUM_ID --exclude 5,6,10,14,15:17,19
    
Exactly the same can be done using `--include`. Easy.

There is also an option available that allows you to include or exclude tracks on multiple discs. To do so you have to specify on which disc you are operating. Let us assume the album id you want to use contains three discs and you want to include tracks 1-10 from the first, tracks 17,19 and 20 from the second and tracks 1-4 from the third disc. You can do this by calling

    python generator.py --album $ALBUM_ID --include cd1-1:10,cd2-17,19,20,cd3-1:4
    
If you do not specify the `cd` tag inside the include/exclude parameter as described in the previous example, the script assumes you to work on the first disc. Again the same scheme can be used for excluding tracks when using the `--exclude` parameter.

### Load artwork
You can also download artwork. To do so you have to specify a destination folder using the `--output` parameter since it must be known where you want to save artwork. You also need to turn on the artwork download option explicit by setting `--cover 1`. So a complete call including cover download could be as in the following example
    
    python generator.py --album $ALBUM_ID --output ~/Desktop/ --cover 1

The artwork download is not part of the Spotify® API. It is possible that the requested artwork can not be found by the program. Then a download is unfortunately not possible and you have to do it by hand.

### Add or substract an offset from each track
You want to cut a file and there is a constant offset between each of the tracks that is not covered by the generated cue-sheet? Try the option `--offset`. Given the offset in seconds (positive real numbers are possible as well as negative real numbers) this option adds (or substracts) a given offset after each track. Check it out if necessary.

Example:

    python generator.py --album $ALBUM_ID --offset 1.2
    
## You found an error?
Let me know if there is something not working as expected and I will try to fix it. Please see the [issues](https://bitbucket.org/dotcs/cueonthespot/issues).

## Remarks
Spotify® is a registered trademark of the Spotify Group.

Please note, that this is a private project. Spotify® is not associated in any way with this project.
