#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
import getopt
import urllib
import HTMLParser
import re
from httplib2 import Http
from BeautifulSoup import BeautifulSoup

# API
##############################################################################
# For API description see: https://developer.spotify.com/technologies/web-api/

"""
Expands a number list that is given in short string notation to its full touple
version which will be returned. 
It is also possible to give single, comma seperated strings but also number ranges 
that have to be devided by a colon.
Example "1:4,7,9:12" will be expanded to List(1,2,3,4,7,9,10,11,12)
"""
def expandList(string):
    subset = str.split(string,",")
    output = []
    for number in subset:
        r = str.split(number,":")
        if len(r) > 1:
            ra = range(int(r[0]),int(r[1])+1)
            output=output + ra
        else:
            try:
                output.append(int(number))
            except ValueError:
                # if the value is not numeric or it is passed an empty value (happens
                # if there is a comma in the end of the given string as the last char)
                pass
    return output

"""
Expands a disc list using the disc number scheme. This function uses expandList() for
each disc track list."
"""
def expandDiscList(string):
    cdSubset = str.split(string,"cd")
    result = dict()
    for cdSub in cdSubset:
        if len(cdSub) == 0:
            continue
        discNumber = cdSub[0:cdSub.index("-")]
        values = cdSub[cdSub.index("-")+1:]
        result[discNumber] = expandList(values)
    return result

""" Expands a list given by the user depending on if it is following the disc
scheme. """
def expandUserList(string):
    if isValidDiscList(string):
        return expandDiscList(string)
    else:
        # fallback: append disc number 1 in order to provide the same structure as with
        # multiple cds
        return expandDiscList("cd1-"+string)

""" Checks if a string is not following in the disc scheme or not. """
def isValidDiscList(string):
    reDisc = r"(cd\d+-[^(cd)]+)+"
    return re.match(reDisc,string) is not None

""" Masks the quote characters in a string by using single quote characters
instead of double quote characters. It is not mentioned how to deal with 
quote characters in the specification of cue sheets. Thus this seems to be 
the best way. """
def maskQuotes(string):
    return string.replace('"', "'")

"""
Converts a XML formatted string to its raw equivalent.
"""
def formatXMLString(string):
    # remove all masked HTML signs
    string = HTMLParser.HTMLParser().unescape(string)
    # mask quotes since they are not allowed in cue sheets
    string = maskQuotes(string)
    return string

def getTrackArtist(track):
    return formatXMLString(track.find("artist").find("name").string)

def isTrackAvailable(track):
    return track.find("available").string == "true"

def getTrackDiscNumber(track):
    return int(track.find('disc-number').string)

def getTrackNumber(track):
    return int(track.find('track-number').string)

def getTrackLength(track):
    return float(track.find('length').string)

def getTrackName(track):
    trackName = formatXMLString(track.find('name').string)
    return trackName

def getTrackMeta(track):
    return {"artist": getTrackArtist(track), "available": isTrackAvailable(track), "disc_number": getTrackDiscNumber(track), "track_number": getTrackNumber(track), "length": getTrackLength(track), "name": getTrackName(track)}

def getAlbumArtist(soup):
    try: 
        name = formatXMLString(soup.find("artist").find("name").string)
    except AttributeError:
        name = "*Unknown*"
    return name

def getAlbumRelease(soup):
    return int(soup.find("released").string)

def getAlbumName(soup):
    return formatXMLString(soup.find("name").string)

def getAlbumIdentifier(album):
    return formatXMLString(album['href'])

def getArtistName(soup):
    return formatXMLString(soup.find("name").string)

def getArtistIdentifier(artist):
    return formatXMLString(artist['href'])

def convertToCueTimestamp(time):
    return "%02d:%02d:%02d" % (time/60, time%60, (time-int(time))*100)

"""
Change the mode to include or exclude mode. This function will raise an
ValueError if the mode is tried to be changed after it is set once or if
a mode is used that is unknown.
"""
def setIncludeExcludeMode(mode):
    global NORMAL_MODE
    global INCLUDE_MODE
    global EXCLUDE_MODE
    global FLAG_INCLUDE_EXCLUDE_MODE

    if FLAG_INCLUDE_EXCLUDE_MODE != NORMAL_MODE:
        raise ValueError("It is not possible to combine include and exclude mode.")

    if mode not in (INCLUDE_MODE, EXCLUDE_MODE, NORMAL_MODE):
        raise ValueError("The requested mode is not valid.")

    FLAG_INCLUDE_EXCLUDE_MODE = mode

""" Generates and returns the cue sheet header depending on the disc metadata, the 
current disc number and the total disc number.
The current disc number and total disc number values are included as a comment (REM)
since they are not officially supported by the cue standard.
"""
def cueSheetHeader(discMeta, discNumber, discNumberMax):
    return 'REM DISCNUMBER %i\nREM TOTALDISCS %i\nREM DATE "%i"\nPERFORMER "%s"\nTITLE "%s"\n' % (discNumber,discNumberMax,discMeta[0],discMeta[1],discMeta[2])

""" Returns the minimum and maximum disc number values stored in the track 
metadata.
"""
def getDiskNumberRange(trackMeta):
    min = None
    max = None
    for track in trackMeta:
        diskNumber = int(track['disc_number'])
        if min == None:
            min = diskNumber
            max = diskNumber
        if diskNumber < min:
            min = diskNumber
        if diskNumber > max:
            max = diskNumber
    return (min, max)

"""
Creates the cue sheet that is returned as a string depending on all the 
user defined settings.
"""
def createCueSheet(discMeta,trackMeta):
    global FLAG_INCLUDE_EXCLUDE_MODE
    global excludeTrackNumbers
    global includeTrackNumbers
    global trackOffset

    (diskMin, diskMax) = getDiskNumberRange(trackMeta)

    cue = ""
    time = 0
    disk = None
    for track in trackMeta:
        if track['available']:
            # include metadata if disc number has changed
            if disk != track['disc_number']:
                disk = track['disc_number']
                cue += cueSheetHeader(discMeta, track['disc_number'], diskMax)
            if FLAG_INCLUDE_EXCLUDE_MODE == INCLUDE_MODE:
                # if the track should not be included, exclude it
                try:
                    if int(track['track_number']) not in includeTrackNumbers[str(disk)]:
                        continue;
                except KeyError: # happens if the cd number was not explicitly mentioned by the user
                    continue
            elif FLAG_INCLUDE_EXCLUDE_MODE == EXCLUDE_MODE:
                try:
                    # if there is the need to exclude a track, exclude it!
                    if int(track['track_number']) in excludeTrackNumbers[str(disk)]:
                        continue
                except KeyError: # happens if the cd number was not explicitly mentioned by the user
                    pass

            cue += 'TRACK %i AUDIO\nTITLE "%s"\nPERFORMER "%s"\nINDEX 01 "%s"\n' % (track['track_number'],track['name'],track['artist'],convertToCueTimestamp(time))
            time += track['length'] + trackOffset
    return cue

"""
Converts a server response into a human readable format.
"""
def getServerResponse(number):
    # source: https://developer.spotify.com/technologies/web-api/
    SERVER_RESPONSES = {200: 'ok', 304: 'not modified', 400: 'bad request', 403: 'forbidden', 404: 'not found', 406: 'not acceptable', 500: 'internal server error', 502: 'bad gateway', 503: 'service unavailable', 504: 'gateway timeout'}
    
    for key, value in SERVER_RESPONSES.iteritems():
        if key == int(number):
            return value

    raise ValueError("The server response with ID %i is unknown." % int(number))

"""
Send a query to the server using a given album identifier. 
Returns the XML code if the request was successfull. Otherwise this function raises an exception.
"""
def loadCueSheet(albumIdentifier):
    global CHARSET
    
    h = Http()
    resp, content = h.request("http://ws.spotify.com/lookup/1/?uri=%s&extras=trackdetail" % albumIdentifier, "GET")
    
    try:
        if getServerResponse(resp['status']) != 'ok':
            raise RuntimeError("""The server did not answer the query in the correct way. The answer ID given by the server is: %i (%s).\n
Try again or create a bug report if you think this is an error of this program.""" % (int(resp['status']), getServerResponse(resp['status'])))
    except ValueError as e:
        message = e.args
        raise RuntimeError("The program has stopped because the server responded with a unknown status. %s" % message[0])
    
    soup = BeautifulSoup(content)
    trackList = soup.findAll("track")
    discMeta = list()
    discMeta.append(getAlbumRelease(soup))
    discMeta.append(getAlbumArtist(soup))
    discMeta.append(getAlbumName(soup))
    trackMeta = list()
    for track in trackList:
        trackMeta.append(getTrackMeta(track))

    return createCueSheet(discMeta,trackMeta).encode(CHARSET)
    
def search(identifier,searchType):
    global SEARCH_ALBUM
    global SEARCH_ARTIST
    
    h = Http()

    if searchType == SEARCH_ALBUM:
        # we have two options: either a album it is searched using the artist prefix and the user
        # wants to list all albums by this artist or after some album name

        # some query using spotify identifier like "spotify:artist:65MWDcUGvGZXts4Y79SWNO"
        if identifier[0:len(SPOTIFY_ARTIST_ID_PREFIX)] == SPOTIFY_ARTIST_ID_PREFIX:
            resp, content = h.request("http://ws.spotify.com/lookup/1/?uri=%s&extras=albumdetail" %  urllib.quote_plus(identifier), "GET")

        # some free form (author) + album query like "Michael jackson invincible"
        else: 
            resp, content = h.request("http://ws.spotify.com/search/1/album?q=%s" %  urllib.quote_plus(identifier), "GET")

        # finaly prepare and show the results
        albums = formatSearchResult(content,SEARCH_ALBUM)
        for album in albums:
            print "%60s\t%40s\t%40s" % (album['name'], album['artist'], album['identifier'])

    elif searchType == SEARCH_ARTIST:
        resp, content = h.request("http://ws.spotify.com/search/1/artist?q=%s" % urllib.quote_plus(identifier), "GET")
        artists = formatSearchResult(content,SEARCH_ARTIST)
        for artist in artists:
            print "%40s\t%40s" % (artist['name'], artist['identifier'])
    
    try:
        if getServerResponse(resp['status']) != 'ok':
            raise RuntimeError("""The server did not answer the query in the correct way. The answer ID given by the server is: %i (%s).\n
Try again or create a bug report if you think this is an error of this program.""" % (int(resp['status']), getServerResponse(resp['status'])))
    except ValueError as e:
        message = e.args
        raise RuntimeError("The program has stopped because the server responded with a unknown status. %s" % message[0])
        

def formatSearchResult(content,searchType):
    if searchType == SEARCH_ALBUM:
        soup = BeautifulSoup(content)
        albums = soup.findAll("album")
        results = list()
        for album in albums:
            results.append({"name": getAlbumName(album), "artist": getAlbumArtist(album), "identifier": getAlbumIdentifier(album)})
        return results

    elif searchType == SEARCH_ARTIST:
        soup = BeautifulSoup(content)
        artists = soup.findAll("artist")
        results = list()
        for artist in artists:
            results.append({"name": getArtistName(artist), "identifier": getArtistIdentifier(artist)})
        return results

def printSheet(cue):
    global CHARSET
    global OUTPUT_PATH

    if OUTPUT_PATH is not None:
        with open(os.path.join(OUTPUT_PATH, OUTPUT_CUE_FILENAME), "w") as text_file:
            text_file.write(cue)
    else:
        print cue

def setTrackOffset(offset):
    global trackOffset
    try:
        trackOffset = float(offset)
    except ValueError:
        raise ValueError("The value given for the track offset has to be a number (float).")

def setVerbose(mode):
    global VERBOSE

    if mode not in (True, False):
        raise ValueError("Verbose mode must be true or false.")
    
    VERBOSE = mode
    return VERBOSE

def verbose():
    global VERBOSE
    return VERBOSE

def setLoadCover(mode):
    global LOAD_COVER 

    if mode not in (True, False):
        raise ValueError("Load cover mode must be true or false.")
    
    LOAD_COVER = mode
    return LOAD_COVER

""" Loads the cover artwork using a given album identifier.
This function will only start its work if OUTPUT_PATH is set because
it needs a destination folder to download the artwork to.

Since the Spotify API does not support artwork downloads the artworks
are downloaded from the corresponding open.spotify.com page. 
As soon as the Spotify API allows to download the artwork via API this
should be changed.

Returns True if the artwork could be loaded and False otherwise. """
def loadCover(albumIdentifier):
    global SPOTIFY_ALBUM_ID_PREFIX
    global OUTPUT_PATH

    if OUTPUT_PATH is None:
        return False

    identifier = albumIdentifier[len(SPOTIFY_ALBUM_ID_PREFIX)+1:len(albumIdentifier)]
    url = "http://open.spotify.com/album/%s" % identifier

    # find out the URL to the image
    h = Http()
    resp, content = h.request(url, "GET")

    if getServerResponse(resp['status']) != 'ok':
        return False

    p = re.compile('img.*?src="(.+?)".*?id="big-cover"')
    m = p.findall(content)
    if not m:
        return False

    # try to load the artwork and save it
    urllib.urlretrieve(m[0], os.path.join(OUTPUT_PATH, OUTPUT_COVER_FILENAME))
    return True

def usage():
    print "---                                                 ---"
    print "--- CueOnTheSpot: Help (version: %5s)             ---" % VERSION
    print "--- written by Fabian Mueller (kontakt@crashsource.de) ---"
    print "---                                                 ---"
    print "Description:"
    print "With this program you can search albums and artists on spotify and generate a cue sheet for single albums. To find an album use the '--search-artist' and '--search-album' parameters. What you need to find out is the corresponding internal spotify album ID ($ALBUM_ID). In a second call this the identifier can be used to generate a cue sheet by calling '--album $ALBUM_ID'.\nPlease note that there are several options to include or exclude tracks from the album and in general the output is printed directly to the terminal. To save the information to a file use the option '--output $DESTINATION' (e.g. '--output ~/Desktop/my_cue_file.cue'). For more information see the detailed usage information below.\n"
    print "Usage:"
    print "-h, --help: Displays this help."
    print "-a, --album $ALBUM_ID: Use the spotify internal ID ($ALBUM_ID) to generate a cue file for this album. The $ALBUM_ID can be found using the '--search-artist' and '--search-album' parameters."
    print "-o, --output: Defines a output file where the generated cue sheet is stored. This option makes only sense in combination with the '--album' parameter.'"
    print "--search-artist: Provides a search function for the artist to find a specific album by this artist."
    print "--search-album: Provides a search function for the albums to find a specific album by its name."
    print "-i, --include: When set only the given, comma seperated track numbers are used to generate the cue file. Ranges can be given with a colon. For example '-i 2:4,13,15:17' includes the tracks 2,3,4,13,15,16,17. Note that it is note possible to combine the include and exclude option."
    print "-e, --exclude: When set the given, comma seperated track numbers are excluded in the generated cue file. The same scheme as for the command '-i' or '--include' can be used. Note that it is not possible to combine the include and exclude option."
    print "--offset: Defines a offset in seconds that is appended after each track. This option can be used to fine tune a generated cue."

def main(argv):                          
    global excludeTrackNumbers
    global includeTrackNumbers 
    global OUTPUT_PATH
    global SEARCH_ARTIST
    global SEARCH_ALBUM
    global INCLUDE_MODE
    global EXCLUDE_MODE

    try:
        opts, args = getopt.getopt(argv[1:], "ho:a:e:i:v:", ["help", "output=", "search-artist=", "search-album=", "album=", "exclude=", "include=", "offset=", "verbose=", "cover="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    albumIdentifier = None
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-o", "--output"): 
            if not os.path.isdir(arg):
                raise ValueError("The parameter output has to be an existing directory")
            OUTPUT_PATH = arg
        elif opt in ("--search-artist"):
            search(arg,SEARCH_ARTIST)
            sys.exit()
        elif opt in ("--search-album"):
            search(arg,SEARCH_ALBUM)
            sys.exit()
        elif opt in ("-i", "--include"):
            setIncludeExcludeMode(INCLUDE_MODE)
            includeTrackNumbers = expandUserList(arg)
        elif opt in ("-e", "--exclude"):
            setIncludeExcludeMode(EXCLUDE_MODE)
            excludeTrackNumbers = expandUserList(arg)
        elif opt in ("-a", "--album"):
            albumIdentifier = arg
        elif opt in ('--offset'):
            setTrackOffset(arg)
        elif opt in ("-v", "--verbose"):
            try:
                setVerbose(bool(int(arg)))
            except ValueError:
                raise ValueError("The parameter 'verbose' has to be either 0 or 1")
        elif opt in ("--cover"):
            try:
                setLoadCover(bool(int(arg)))
            except ValueError:
                raise ValueError("The parameter 'cover' has to be either 0 or 1")

    if (albumIdentifier is not None):
        cueSheet = loadCueSheet(albumIdentifier)
        if LOAD_COVER:
            loadCover(albumIdentifier)
        printSheet(cueSheet)
        sys.exit()

# version
VERSION = '0.1.1'

# charset/language
CHARSET = "utf-8"

# verbose mode
VERBOSE = False

# load cover mode
LOAD_COVER = False

# output
OUTPUT_PATH = None
OUTPUT_CUE_FILENAME = "album.cue"
OUTPUT_COVER_FILENAME = "cover.jpg"

# spotify internal settings
SPOTIFY_ARTIST_ID_PREFIX = "spotify:artist"
SPOTIFY_ALBUM_ID_PREFIX = "spotify:album"

# search modes
SEARCH_ARTIST = 1
SEARCH_ALBUM = 2

# include/exclude modes
NORMAL_MODE = 1
INCLUDE_MODE = 2
EXCLUDE_MODE = 4
FLAG_INCLUDE_EXCLUDE_MODE = NORMAL_MODE

# internal structures
excludeTrackNumbers = dict() # track number that should be excluded from the cue file
includeTrackNumbers = dict() # track numbers taht should be includes / exclusive option to exclude tracks
trackOffset = 0.0 # trackOffset in seconds (added to each track)

if __name__ == "__main__":
    main(sys.argv)
